# Antdid.ThisCli

Source generator for retrieving assembly/csproj property information without reflection.

## Installation

Download the [.NET SDK 8.0](https://dotnet.microsoft.com/download/dotnet/current) or later.

Add the project registry package as a nuget source
```
dotnet nuget add source "https://gitlab.com/api/v4/projects/40800082/packages/nuget/index.json" --name "gitlab-antdid"
```

In your project : 

```
dotnet add package Antdid.ThisCli
```

## Usage

```cs
ThisCli.Company;
ThisCli.ToolCommandName;
ThisCli.Name;
ThisCli.Version;
ThisCli.Description;
```

Varies depending on available information.


## Credit 

[ThisAssembly.Project](https://github.com/devlooped/ThisAssembly#thisassemblyproject)
