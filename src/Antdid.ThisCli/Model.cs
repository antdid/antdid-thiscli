﻿using System.Reflection;

namespace AntdidThisCli
{
    internal class Model
    {
        public string Version => Assembly.GetExecutingAssembly().GetName().Version.ToString(3);

        public Model(IEnumerable<KeyValuePair<string, string>> properties) => Properties = properties.ToList();

        public List<KeyValuePair<string, string>> Properties { get; }
    }
}
