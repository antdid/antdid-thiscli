﻿using System.Collections.Immutable;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using Scriban;

namespace AntdidThisCli
{
    [Generator(LanguageNames.CSharp)]
    internal class ThisCliGenerator : IIncrementalGenerator
    {
        public void Initialize(IncrementalGeneratorInitializationContext context)
        {
            var properties = context.AnalyzerConfigOptionsProvider
                .SelectMany((p, ct) =>
                {
                    var go = p.GlobalOptions;
                    if (!go.TryGetValue("build_property.ThisCliProject", out var values))
                        return Array.Empty<KeyValuePair<string, string?>>();

                    return values.Split('|')
                        .Select(prop => new KeyValuePair<string, string?>(
                            prop.Replace("Assembly", ""),
                            go.TryGetValue("build_property." + prop, out var value) ? value : null))
                        .Where(pair => pair.Value != null);
                })
                .Collect();

            context.RegisterSourceOutput(properties, GenerateSource);

        }

        void GenerateSource(SourceProductionContext spc, ImmutableArray<KeyValuePair<string, string>> properties)
        {
            var model = new Model(properties);

            // Build up the source code
            var template = Template.Parse(@"
// </auto-generated>

using System;
using System.CodeDom.Compiler;
using System.Runtime.CompilerServices;

/// <summary>
/// Provides access to the current csproj information as pure constants without requiring reflection.
/// </summary>
[GeneratedCode(""Antdid.ThisCli"", ""{{ Version }}"")]
[CompilerGenerated]
internal static partial class ThisCli
{
    {{~ for prop in Properties ~}}
    /// <summary>{{ prop.Key }} = {{ prop.Value }}</summary>
    public const string {{ prop.Key }} = @""{{ prop.Value }}"";
    {{~ end ~}}
}
");

            var result = template.Render(model, member => member.Name);

            spc.AddSource("ThisCli.Property.g.cs", SourceText.From(result, Encoding.UTF8));
        }
    }
}
