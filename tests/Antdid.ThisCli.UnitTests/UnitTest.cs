namespace AntdidThisCli.UnitTests
{
    public class UnitTest
    {
        [Fact]
        public void TestSameAsCsprojInfo()
        {
            Assert.Equal("companyName", ThisCli.Company);
            Assert.Equal("toolName", ThisCli.ToolCommandName);
            Assert.Equal("customDescription", ThisCli.Description);
            Assert.Equal("1.0.0", ThisCli.Version);
            Assert.Equal("Antdid.ThisCli.UnitTests", ThisCli.Name);
            Assert.Equal("Antdid", ThisCli.Authors);
        }
    }
}
